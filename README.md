# tf-aws-acm

This will create an ACM certificate in a specified region and creates the Route 53 DNS records if DNS validation is selected and if the DNS Zone File is located in AWS Route 53.

## Terraform version compatibility

| Module version    | Terraform version |
|-------------------|-------------------|
| 3.x.x             | 1.5.5             |
| 2.x.x             | 0.15.x            |
| 1.x.x             | 0.12.x            |
| 0.2.1 and earlier | 0.11.x            |

Upgrading from 0.11.x and earlier to 0.12.x should be seamless.  You can simply update the `ref` in your `source` to point to a version greater than `1.0.0`.

## Usage

You always have to pass `providers` block with two providers:
- `aws.acm` - for ACM related resources.
- `aws.r53` - for DNS related resources

In case you want all resources created with the same provider simply pass the same provider:

```
providers = {
  aws.acm = "aws"
  aws.r53 = "aws"
}
```

If `wait_for_validation = false` is used it is assumed that user will check if the certificate has been validated before using it.


### Certificate with a single domain

```js
provider "aws" {
  alias  = "dns-account"

  assume_role {
    role_arn = "arn:aws:iam::123345678901:role/dns-management-role"
  }
}


module "acm_certificate" {
  source = "../"

  domains = [
    {
      name    = "trynotto.click"
      zone_id = "ZW7HC3OXIT5P9"
    }
  ]

  domains_count = 1

  # different providers for ACM and DNS resources
  providers = {
    aws.acm = "aws"
    aws.r53 = "aws.dns-account"
  }
}
```

### Certificate in a different region

```js
provider "aws" {
  alias  = "cloudfront"
  region = "us-east-1"
}


module "acm_certificate" {
  source = "../"

  domains = [
    {
      name    = "trynotto.click"
      zone_id = "ZW7HC3OXIT5P9"
    }
  ]

  domains_count = 1

  # different providers for ACM and DNS resources
  providers = {
    aws.acm = "aws.cloudfront"
    aws.r53 = "aws"
  }
}
```

### Certificate with multiple domains

```js
module "acm_certificate" {
  source = "../"

  domains = [
    {
      name    = "trynotto.click"
      zone_id = "ZW7HC3OXIT5P9"
    },
    {
      name    = "api.trynotto.click"
      zone_id = "ZW7HC3OXIT5P9"
    },
    {
      name    = "www.trynotto.click"
      zone_id = "ZW7HC3OXIT5P9"
    },
    {
      name    = "stage.trynotto.click"
      zone_id = "Z33RTPZPQU0IR5"
    },
    {
      name    = "api.stage.trynotto.click"
      zone_id = "Z33RTPZPQU0IR5"
    },
    {
      name    = "www.stage.trynotto.click"
      zone_id = "Z33RTPZPQU0IR5"
    },
  ]

  domains_count = 6

  # the same provider for ACM and DNS resources
  providers = {
    aws.acm = "aws"
    aws.r53 = "aws"
  }
}
```
### Certificate with email validation

```js
module "acm_certificate" {
  source = "../"

  domains = [
    {
      name = "trynotto.click"
    },
    {
      name = "api.trynotto.click"
    },
    {
      name = "www.trynotto.click"
    },
    {
      name = "stage.trynotto.click"
    },
    {
      name = "api.stage.trynotto.click"
    },
    {
      name = "www.stage.trynotto.click"
    },
  ]

  domains_count = 6

  wait_for_validation = false

  validation_method = "EMAIL"

  providers = {
    aws.acm = "aws"
    aws.r53 = "aws"
  }
}
```

## References

The following AWS Documentation was used as references:

https://www.terraform.io/docs/providers/aws/r/acm_certificate.html

https://www.terraform.io/docs/providers/aws/r/acm_certificate_validation.html

https://www.terraform.io/docs/providers/aws/d/acm_certificate.html

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws.acm"></a> [aws.acm](#provider\_aws.acm) | n/a |
| <a name="provider_aws.r53"></a> [aws.r53](#provider\_aws.r53) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.cert](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_acm_certificate_validation.certificate_validation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation) | resource |
| [aws_route53_record.dns_validation_record](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_dns_records"></a> [create\_dns\_records](#input\_create\_dns\_records) | Create the DNS records in Route53. | `bool` | `true` | no |
| <a name="input_domains"></a> [domains](#input\_domains) | List of maps of domains to associate with the new certificate. For example: domains = [ { name = "www.example.com"  zone\_id = "Z123456789" } ] | `list(map(string))` | n/a | yes |
| <a name="input_domains_count"></a> [domains\_count](#input\_domains\_count) | Length of domains list - because count cannot be calculated on computed values. | `any` | n/a | yes |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Enable or disable the resources. | `bool` | `true` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags for the certificate. | `map(string)` | `{}` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | The length of time in minutes Terraform should wait for to allow AWS to validate the ACM Certificate. | `string` | `"45"` | no |
| <a name="input_ttl"></a> [ttl](#input\_ttl) | The time-to-live for the DNS record. | `string` | `"60"` | no |
| <a name="input_validation_method"></a> [validation\_method](#input\_validation\_method) | The method of validation for the ACM Cert. The allowed values are DNS and EMAIL | `string` | `"DNS"` | no |
| <a name="input_wait_for_validation"></a> [wait\_for\_validation](#input\_wait\_for\_validation) | Wait for the certificate to be validated. | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_certificate_arn"></a> [certificate\_arn](#output\_certificate\_arn) | The ARN of the AWS ACM Certificate. |
| <a name="output_dns_validation_records"></a> [dns\_validation\_records](#output\_dns\_validation\_records) | A list of DNS records required for DNS Validation of the ACM Certificate. |
