locals {
  domain_names             = [for d in var.domains : d.name]
  zone_ids                 = [for d in var.domains : d.zone_id]
  domains_and_zone_ids_map = zipmap(local.domain_names, local.zone_ids)
}

resource "aws_acm_certificate" "cert" {
  count                     = var.enabled ? 1 : 0
  provider                  = aws.acm
  domain_name               = var.domains[0].name
  subject_alternative_names = slice(local.domain_names, 1, length(local.domain_names))
  validation_method         = var.validation_method
  tags                      = var.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "dns_validation_record" {
  count           = var.enabled && var.create_dns_records && var.validation_method == "DNS" ? length(tolist(aws_acm_certificate.cert[0].domain_validation_options)) : 0
  provider        = aws.r53
  allow_overwrite = true
  zone_id         = local.domains_and_zone_ids_map[tolist(aws_acm_certificate.cert[0].domain_validation_options)[count.index].domain_name]
  name            = tolist(aws_acm_certificate.cert[0].domain_validation_options)[count.index].resource_record_name
  type            = tolist(aws_acm_certificate.cert[0].domain_validation_options)[count.index].resource_record_type
  ttl             = var.ttl

  records = [
    tolist(aws_acm_certificate.cert[0].domain_validation_options)[count.index].resource_record_value,
  ]
}

resource "aws_acm_certificate_validation" "certificate_validation" {
  count           = var.enabled && var.wait_for_validation ? 1 : 0
  provider        = aws.acm
  certificate_arn = aws_acm_certificate.cert[0].arn

  timeouts {
    create = "${var.timeout}m"
  }
}
